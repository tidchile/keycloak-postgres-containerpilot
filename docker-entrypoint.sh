#!/bin/bash

if [ $KEYCLOAK_USER ] && [ $KEYCLOAK_PASSWORD ]; then
  keycloak/bin/add-user-keycloak.sh --user $KEYCLOAK_USER --password $KEYCLOAK_PASSWORD
fi

/opt/tidchile/containerpilot /opt/jboss/keycloak/bin/standalone.sh -b 0.0.0.0
