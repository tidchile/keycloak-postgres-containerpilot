# Keycloak PostgreSQL ContainerPilot image

Extends the Keycloak PostgreSQL docker image with [ContainerPilot](https://www.joyent.com/containerpilot) support. 

# Usage

## Environment variables

### CONSUL_HOST
Specify name or address of Consul service (optional, default is `consul`)

### CONSUL_PORT
Specify port number of Consul service (optional, default is `8500`)

# Base images usage

See specific usage instructions for:
* [Keycloak bsae docker image](https://hub.docker.com/r/jboss/keycloak/).
* [Keyclock PostgreSQL base docker image](https://hub.docker.com/r/jboss/keycloak-postgres/). 

