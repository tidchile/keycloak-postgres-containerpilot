FROM jboss/keycloak-postgres
MAINTAINER Ricardo Stuven <ricardo.stuven@telefonica.com>

USER root
RUN mkdir /opt/tidchile && \
    chown jboss:jboss /opt/tidchile
USER jboss

# get ContainerPilot release
ENV CONTAINERPILOT_VERSION 2.4.4
ENV CP_SHA1 6194ee482dae95844046266dcec2150655ef80e9
RUN curl -Lso /tmp/containerpilot.tar.gz \
      "https://github.com/joyent/containerpilot/releases/download/${CONTAINERPILOT_VERSION}/containerpilot-${CONTAINERPILOT_VERSION}.tar.gz" && \
    echo "${CP_SHA1} /tmp/containerpilot.tar.gz" | sha1sum -c && \
    tar -zxf /tmp/containerpilot.tar.gz -C /opt/tidchile && \
    rm /tmp/containerpilot.tar.gz

# add ContainerPilot configuration and scripts
COPY docker-entrypoint.sh /opt/tidchile/
COPY containerpilot.json /opt/tidchile/
ENV CONTAINERPILOT=file:///opt/tidchile/containerpilot.json
ENV CONSUL_HOST consul
ENV CONSUL_PORT 8500

ENTRYPOINT [ "/opt/tidchile/docker-entrypoint.sh" ]
